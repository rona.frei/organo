import './Rodape.css';

const Rodape = () => {
    return (
        <footer className='footer'>
            <section>
                <ul>
                    <li>
                        <a href="https://facebook.com">
                            <img src = "/imagens/fb.png" alt=""/>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com">
                        <img src = "/imagens/tw.png" alt=""/>
                        </a>
                    </li>
                    <li>
                        <a href="https://instagram.com">
                            <img src = "/imagens/ig.png" alt=""/>
                        </a>
                    </li>
                </ul>    
            </section>
            <section>
                <img src = "/imagens/logo.png" alt=""></img>
            </section>
            <section>
                <a href='https://www.linkedin.com/in/ronafrei/'>
                    <p>Desenvolvido por Ronaldo Freitas</p> 
                </a>
            </section>
        </footer>
    )
}

export default Rodape;
